<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\MyController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\MyService\Facades\Btn;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => 'true']);
Route::group(['middleware' => 'verified'], function () {

    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    Route::resource('customers', CustomerController::class)->middleware('auth');

    Route::resource('users', UserController::class)->middleware('auth');
});

Route::get('generate-pdf/{customer}', [PDFController::class, 'generatePDF']);

Route::get('importExportView', [MyController::class, 'importExportView']);

Route::get('export', [MyController::class, 'export'])->name('export');

Route::post('import', [MyController::class, 'import'])->name('import');


Route::get('/myservice', function () {
    
    return Btn::a('/home','btn btn-outline-danger');
});
