<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exports\CustomersExport;
use App\Imports\CustomersImport;
use Maatwebsite\Excel\Facades\Excel;

class MyController extends Controller
{
    /**

     * @return \Illuminate\Support\Collection

     */

    public function importExportView()

    {

        return view('import');
    }



    /**

     * @return \Illuminate\Support\Collection

     */

    public function export()

    {

        return Excel::download(new CustomersExport, 'users.xlsx');
    }



    /**

     * @return \Illuminate\Support\Collection

     */

    public function import()

    {
        if(request()->file('file')){

            $collection = request()->file('file');

            Excel::import(new CustomersImport, $collection);
            
            return back()->with('success', 'Fichero importado con éxito');
        }   
        



        return back()->with('error', 'Error en la importación, el campo esta vacio');;
    }
}
