<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade\Pdf;
use App\Models\Customer;

class PDFController extends Controller
{
    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function generatePDF(Customer $customer = null)
    {

        
        $data = [

            'title' => $customer->name,

            'date' => date('m/d/Y'),

            'email' => $customer->email,

            'phone' => $customer->phone,

            'address' => $customer->address

        ];



        $pdf = Pdf::loadView('myPDF', $data);



        return $pdf->download('itsolutionstuff.pdf');
    }
}
