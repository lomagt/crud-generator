<?php

namespace App\MyService;

class Btn
{
    public function a($url, $text = null)// funcion que recoge dos parámetros, uno de ellos puesto a null por defecto
    {
        return view('btn.a', [ // retorna a la vista en la carpeta btn y dentro la vista a
            'url' => url($url), // manda un array asociativo con una url, usando la función de laravel url() que genera una url 
                                // arbitraria que usará el host de la solicitud y el protocolo https o http
            'text' => is_null($text) ? $url : $text // comprueba si es null y si lo es asocia $url con text y si no asocia $text
        ]);
    }
}
